import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from 'src/app/model/user.model';
import { UserService } from 'src/app/service/user.service';
import { NullValidationHandler, OAuthService, AuthConfig } from 'angular-oauth2-oidc';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{
  public isLoggedIn = false;
  public userProfile: OAuthService | null = null;

  private subscription: Subscription = new Subscription;
  user = new User() ;
  constructor(private oauthService: OAuthService, private _userService: UserService) {
    this.configure();
  }


  public async ngOnInit() {

    this.isLoggedIn = this.oauthService.hasValidAccessToken();
    // this.getUserById();
  }


  //#################################################
  authConfig: AuthConfig = {
    issuer: 'http://localhost:8080/realms/Demo',
    redirectUri: window.location.origin + "/login",
    clientId: 'demo',
    scope: 'openid profile email',
    responseType: 'code',
    requireHttps: false,
    // at_hash is not present in JWT token
    disableAtHashCheck: true,
    showDebugInformation: true
  }
//#################################################



  getUserById(): void {
    this.subscription = this._userService.getUserById().subscribe({
      next: (data: any) => {
        // Handle successful response, if needed
        this.user.username= data.username;
        this.user.email= data.email;
      },
      error: (error: any) => {
        console.error('Error getUserById:', error);
        console.error(error);
      },    // errorHandler 
    });
  }

  public login() {
    this.oauthService.initLoginFlow();
  }

  public logout() {
    this.oauthService.logOut();
  }

  getUserMessage(): void {
    this.subscription = this._userService.getUserMessage().subscribe(res => {
      console.log(res)
    })
    // const user = this.oauthService. loadUserProfile();
    // console.log(user);
  }
 
  //#################################################不確定是不是必要
  private configure() {
    this.oauthService.configure(this.authConfig);
    this.oauthService.tokenValidationHandler = new NullValidationHandler();
    this.oauthService.loadDiscoveryDocumentAndTryLogin();
  }
  //#################################################

}


