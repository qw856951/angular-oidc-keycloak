import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Message, User } from '../model/user.model';

// Define the HTTP headers if needed (e.g., for JSON content)
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  // mock a api get name ang email
  
  getUserById(): Observable<any> {
    return this.http.get<User>('/api/users/1', httpOptions);
  }

  getUserMessage(): Observable<any> {
    return this.http.get<Message>('/api/message', httpOptions);
  }
}
