export class User {
    id: number | undefined;
    username: string | undefined;
    email: string | undefined;
}

export class Message {
    text: string | undefined;
}